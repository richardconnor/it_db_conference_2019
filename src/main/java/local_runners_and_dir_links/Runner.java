package local_runners_and_dir_links;

import stringVectorisation.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;
import java.util.Map.Entry;

public class Runner {

	public static void main(String[] args) throws IOException {

		try {
			assert false;
			throw new RuntimeException("assertions are disabled");
		} catch (AssertionError e) {
			System.out.println("assertions are enabled");
		}

		// runExperiments();
		printOutputForPaper();
		// getDictStats();
		// getDictExamples();
	}

	private static void getDictStats() throws IOException {
		List<String> testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		int acc = 0;
		for (String s : testLoad) {
			acc += s.length();
		}
		System.out.println(testLoad.size());
		System.out.println(acc / (float) testLoad.size());
	}

	@SuppressWarnings("boxing")
	private static void getDictExamples() throws IOException {
		Random r = new Random(100);
		List<String> testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		Metric<String> lev = new Levenshtein();
		Metric<String> jsd = new JS(256);

		StringBuffer[] lines = new StringBuffer[11];
		for (int i = 0; i < lines.length; i++) {
			lines[i] = new StringBuffer();
		}

		String[] test = { "string", "metric", "simplex", "error" };
		for (int i = 0; i < 4; i++) {

			TreeMap<Double, String> map1 = new TreeMap<>();
			TreeMap<Double, String> map2 = new TreeMap<>();
			String rand = test[i];
			for (String s : testLoad) {
				map1.put(lev.distance(s, rand), s);
				map2.put(jsd.distance(s, rand), s);
			}
			lines[0].append(rand + "\t&\t&");
			int ptr = 1;
			for (Entry<Double, String> d : map1.entrySet()) {
				if (ptr < 11) {
					lines[ptr++].append(d.getValue() + "\t&");
				}
			}
			ptr = 1;
			for (Entry<Double, String> d : map2.entrySet()) {
				if (ptr < 11) {
					lines[ptr++].append(d.getValue() + "\t&");
				}
			}
		}
		for (StringBuffer line : lines) {
			System.out.println(line.toString() + "\t\\\\");
		}

	}

	private static void printOutputForPaper() {
		System.out.println("\nlev rel errors");
		printDataForPaper("lev", "relError");
		System.out.println("\nlev sq errors");
		printDataForPaper("lev", "sqError");
		System.out.println("\nlev distortions");
		printDataForPaper("lev", "distortion");
		System.out.println("\njsd rel errors");
		printDataForPaper("jsd", "relError");
		System.out.println("\njsd sq errors");
		printDataForPaper("jsd", "sqError");
		System.out.println("\njsd distortions");
		printDataForPaper("jsd", "distortion");
	}

	private static void printDataForPaper(String groundMetric, String attribute) {
		for (int dim : ErrorTest.dims) {
			System.out.print(dim);
			for (String exp : ErrorTest.experiments) {
				final String localFileName = ErrorTest.getFileName(dim, groundMetric, exp);
				String filename = LocalDirectoryLinks.rootOutputDir + localFileName;
				try {
					LineNumberReader lnr = new LineNumberReader(new FileReader(filename));
					System.out.print("\t&");
					outputFieldForFile(attribute, localFileName, lnr);
					lnr.close();
				} catch (FileNotFoundException e) {
					System.out.println("cannot open file " + filename);
				} catch (IOException e) {
					System.out.println("failed to read or close file " + filename);
				}
			}
			System.out.println("\t\\\\");
		}

	}

	private static void outputFieldForFile(String attribute, String test, LineNumberReader lnr) throws IOException {
		/*
		 * cols 4,5 and 5 in the tables
		 */
		List<Double> relErr = new ArrayList<>();
		List<Double> sqErr = new ArrayList<>();
		List<Double> distortions = new ArrayList<>();
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			try {
				Scanner s = new Scanner(line);
				s.useDelimiter("(,|\\s)");
				s.nextDouble();
				s.nextDouble();
				s.nextDouble();
				relErr.add(s.nextDouble());
				sqErr.add(s.nextDouble());
				distortions.add(s.nextDouble());
			} catch (Throwable t) {
				System.out.println("scanner failed: " + line);
			}
		}

		double meanRel = ErrorValues.median(relErr);
		double meanSq = ErrorValues.median(sqErr);
		double meanDistortion = ErrorValues.median(distortions);
		double thisAtt = attribute.equals("relError") ? meanRel
				: attribute.equals("sqError") ? meanSq : attribute.equals("distortion") ? meanDistortion : -1;

		System.out.print(round(thisAtt));

		if (attribute.equals("distortion")) {
			double eps = (meanDistortion - 1) / (meanDistortion + 1);
			System.out.print("\t" + round(eps));
		}
	}

	private static void runExperiments() {
		List<String> testLoad = null;
		try {
			testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		} catch (IOException e1) {
			System.out.println("cannot open dictionary file at " + LocalDirectoryLinks.dicBase);
		}

		ErrorTest et = new ErrorTest(testLoad, LocalDirectoryLinks.rootOutputDir);

		try {
			et.runTestsForPaper();
		} catch (FileNotFoundException e) {
			System.out.println("cannot open output files at " + LocalDirectoryLinks.rootOutputDir);
		} catch (IOException e) {
			System.out.println("cannot write to output files at " + LocalDirectoryLinks.rootOutputDir);
		}
	}

	private static String round(double d) {
		String s = Double.toString((float) d);
		boolean stop = false;
		StringBuffer sb = new StringBuffer();
		if (d < 1e-3) {
			return s;
		}
		int strPtr = 0;
		while (!stop) {
			sb.append(s.charAt(strPtr++));
			stop = strPtr > 5 || strPtr == s.length();
		}
		return sb.toString();
	}

}
