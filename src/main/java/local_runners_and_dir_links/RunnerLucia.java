package local_runners_and_dir_links;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import stringVectorisation.ErrorTestLucia;
import stringVectorisation.ErrorValues;
import stringVectorisation.JS;
import stringVectorisation.Levenshtein;
import stringVectorisation.Metric;
import stringVectorisation.StringMisc;

public class RunnerLucia {
 
	public static void main(String[] args) throws IOException {     
		try {
			assert false;
			throw new RuntimeException("assertions are disabled");
		} catch (AssertionError e) {
			System.out.println("assertions are enabled");
		}

//			runExperiments();
	printOutputForPaper();
//		getDictStats();
//		getDictExamples();
	}

	private static void getDictStats() throws IOException {
		List<String> testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		int acc = 0;
		for (String s : testLoad) {
			acc += s.length();
		}
		System.out.println(testLoad.size());
		System.out.println(acc / (float) testLoad.size());
	}

	@SuppressWarnings("boxing")
	private static void getDictExamples() throws IOException {
		Random r = new Random(100);
		List<String> testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		Metric<String> lev = new Levenshtein();
		Metric<String> jsd = new JS(256);

		StringBuffer[] lines = new StringBuffer[11];
		for (int i = 0; i < lines.length; i++) {
			lines[i] = new StringBuffer();
		}

		String[] test = {"string","metric","simplex","error"};
		for (int i = 0; i < 4; i++) {
			
			TreeMap<Double, String> map1 = new TreeMap<>();
			TreeMap<Double, String> map2 = new TreeMap<>();
			String rand = test[i];
			for (String s : testLoad) {
				map1.put(lev.distance(s, rand), s);
				map2.put(jsd.distance(s, rand), s);
			}
			lines[0].append(rand + "\t&\t&");
			int ptr = 1;
			for (Entry<Double, String> d : map1.entrySet()) {
				if (ptr < 11) {
					lines[ptr++].append(d.getValue() + "\t&");
				}
			}
			ptr = 1;
			for (Entry<Double, String> d : map2.entrySet()) {
				if (ptr < 11) {
					lines[ptr++].append(d.getValue() + "\t&");
				}
			}
		}
		for (StringBuffer line : lines) {
			System.out.println(line.toString() + "\t\\\\");
		}

	}

	private static void printOutputForPaper() {
			StringBuilder sb= new StringBuilder();
			sb.append("pivots");
			for (String exp : ErrorTestLucia.exps) {
				sb.append("\t&"+exp);
			}
			sb.append("\\\\");
			
			System.out.println("\nlev rel errors");
			System.out.println(sb);
			printDataForPaper("lev", "relError");
			
			System.out.println("\nlev sq errors");
			System.out.println(sb);
			printDataForPaper("lev", "sqError");
			
			System.out.println("\nlev distortions");
			System.out.println(sb);
			printDataForPaper("lev", "distortion");
			
			System.out.println("\njsd rel errors");
			System.out.println(sb);
			printDataForPaper("jsd", "relError");
			
			System.out.println("\njsd sq errors");
			System.out.println(sb);
			printDataForPaper("jsd", "sqError");
			
			System.out.println("\njsd distortions");
			System.out.println(sb);
			printDataForPaper("jsd", "distortion");
	
		
	}


	private static void printDataForPaper(String groundMetric, String attribute) {
		for (int dim : ErrorTestLucia.dims) {
			System.out.print(dim);
			for (String exp : ErrorTestLucia.exps) {
				final String localFileName = ErrorTestLucia.getFileName(dim, groundMetric, exp);
				String filename = LocalDirectoryLinks.rootOutputDir + localFileName;
				try {
					
					LineNumberReader lnr = new LineNumberReader(new FileReader(filename));
					lnr.readLine(); //header
					System.out.print("\t&");
					System.out.print(round(outputFieldForFile(attribute, localFileName, lnr)));
					lnr.close();
					lnr = new LineNumberReader(new FileReader(filename));
					lnr.readLine(); //header
					int projDim=(int) outputFieldForFile("ProjDims", localFileName, lnr);
					if(projDim!=dim) 
						System.out.print(" ("+projDim+ ")");
					lnr.close();
				} catch (FileNotFoundException e) {
					System.out.println("cannot open file " + filename);
				} catch (IOException e) {
					System.out.println("failed to read or close file " + filename);
				}
			}
			System.out.println("\t\\\\");
		}

	}

	private static double outputFieldForFile(String attribute, String test, LineNumberReader lnr) throws IOException {
		/*
		 * cols 5,6and 5 in the tables
		 */
		List<Double> projDims = new ArrayList<>();
		List<Double> relErr = new ArrayList<>();
		List<Double> sqErr = new ArrayList<>();
		List<Double> distortions = new ArrayList<>();
		for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
			try {
				String [] split=line.split(",");
				double minRatio=Double.parseDouble(split[2]);
				 //don't know why the scanner is not working for me..I got scanner failed..
				if(minRatio> Math.pow(10, -3)) { //excluding pathological cases
					projDims.add(Double.parseDouble(split[0]));
					relErr.add(Double.parseDouble(split[4]));
					sqErr.add(Double.parseDouble(split[5]));
					distortions.add(Double.parseDouble(split[6]));
				}
			} catch (Throwable t) {
				System.out.println("scanner failed: " + line);
			}
		}
		double meanDims = ErrorValues.mean(projDims);
		double meanRel = ErrorValues.mean(relErr);
		double meanSq = ErrorValues.mean(sqErr);
		double meanDistortion = ErrorValues.mean(distortions);
		double thisAtt = attribute.equals("relError") ? meanRel
				: attribute.equals("sqError") ? meanSq : attribute.equals("distortion") ? meanDistortion : attribute.equals("ProjDims") ? meanDims
						: -1;
	
		return thisAtt;
	}

	private static void runExperiments() {
		List<String> testLoad = null;
		try {
			testLoad = StringMisc.getDict(LocalDirectoryLinks.dicBase);
		} catch (IOException e1) {
			System.out.println("cannot open dictionary file at " + LocalDirectoryLinks.dicBase);
		}
		ErrorTestLucia etLucia = new ErrorTestLucia(testLoad, LocalDirectoryLinks.rootOutputDir);
		try {
			etLucia.runTestsForPaper(20);
		} catch (FileNotFoundException e) {
			System.out.println("cannot open output files at " + LocalDirectoryLinks.rootOutputDir);
		} catch (IOException e) {
			System.out.println("cannot write to output files at " + LocalDirectoryLinks.rootOutputDir);
		}
	}

	private static String round(double d) {
		String s = Double.toString((float) d);
		boolean stop = false;
		StringBuffer sb = new StringBuffer();
		if (d < 1e-3) {
			return s;
		}
		int strPtr = 0;
		while (!stop) {
			sb.append(s.charAt(strPtr++));
			stop = strPtr > 5 || strPtr == s.length();
		}
		return sb.toString();
	}

}
