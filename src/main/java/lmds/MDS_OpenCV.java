package lmds;

import org.opencv.core.Core;
import org.opencv.core.Mat;

/**
 * @author Lucia Vadicamo
 * 
 * 
 */
public class MDS_OpenCV <T>  {

	static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
	

	public static final PrincipalComponents compute(Mat squaredDistMatrix) throws Exception {
		// Construct the mean-centered “inner-product” matrix B=-0.5 H D2 H, where
		// D2=squaredDistMatrix=[(dij)^2]
		//H= the mean-centering matrix, defined by [H]ij = (dij)^2 − 1/n.
		//it can be proved that
		// B=[bij], bij= -0.5((dij)^2- means[i] -means[j] +mu)
		//where means[i]= mean( D2(i,:)); mu=mean(muVec[:])
		int n=squaredDistMatrix.rows();
		
		Mat B = squaredDistMatrix.clone();	

		double[] means=new double[n];
		double mu=0;
		for (int i = 0; i < n; i++) {
			double imean=0;
		    for (int j = 0; j < n; j++) {
		    	double[] val=squaredDistMatrix.get(i, j);
		    	imean+=val[0];
		    }
		    means[i]=imean/n;
		    mu+=means[i];
		}
		mu/=n;
		
		for (int i = 0; i < n; i++) 
		    for (int j = 0; j < n; j++) {
		    	double[] val=squaredDistMatrix.get(i, j);
		    	double bij= -0.5*(val[0]-means[i]-means[j]+mu);	
		    	val[0]=bij;
		    	B.put(i, j, val);
		    	}
		
//OLD code
//		//compute H 
//		Mat H = squaredDistMatrix.clone();	
//		int n=H.rows();
//		for (int i = 0; i < H.rows(); i++) {
//		    for (int j = 0; j < H.cols(); j++) {
//		    	double[] val=squaredDistMatrix.get(i, j);
//		    	val[0]=(val[0])-1.0/n; ERR deltaij-1/n!!!
//		        H.put(i, j, val);
//		    }
//		}
//
//		Mat B = new Mat();
//		Mat tmp = new Mat();
//		org.opencv.core.Core.gemm(squaredDistMatrix, H, 1, new Mat(), 0, tmp);
//		org.opencv.core.Core.gemm(H, tmp, -0.5, new Mat(), 0, B);
			
//		Mat col_mean= new Mat();
//		org.opencv.core.Core.reduce(squaredDistMatrix,col_mean, 1, Core.REDUCE_AVG);
//		double[] means = MatCONV.getDoublesRows(col_mean);
		
		
		Mat eigenvectors  = new Mat();
		Mat eigenvalues  = new Mat();
		
		//Log.info("Starting OpenCV_eigenDecomposition on " + B.rows() + " x " + B.cols()+" matrix" );
		org.opencv.core.Core.eigen(B, true, eigenvalues, eigenvectors);
//		System.out.println("OpenCV Mat data:\n" + eigenvalues.dump());
		B=null;
		
		double[][] eigenvectors_res = MatConversion.getDoubleMatrix(eigenvectors);
		double[] eigenvalues_res=MatConversion.getDoublesRows(eigenvalues);
		
		return new PrincipalComponents(eigenvectors_res, means, eigenvalues_res);
		
	}
	
//	public static final PrincipalComponents compute(
//			FeaturesCollectorsArchive fc,
//			int nMaxObj, ISimilarity sim ) throws Exception {
//		
//		Mat squaredDistances = null;
//
//		squaredDistances = MatCONV.getSquaredDistFromFC(fc, nMaxObj,sim); 
//		
//		return  compute(squaredDistances);
//		
//	}
	

	
}
