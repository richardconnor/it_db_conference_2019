package lmds;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;



public class PrincipalComponents {

	double[][] eigenVectors;
	double[] eigenValues;
	double[] means;
		
	public double[] getEigenValues() {
		return eigenValues;
	}
	
	public double[] getMeans() {
		return means;
	}
	
	public double[][] getPrincipalComponents() {
		return eigenVectors;
	}

	
	
	public PrincipalComponents(double[][] eigenVectors, double[] mean, double[] eigenValues) {
		this.eigenVectors = eigenVectors;
		this.means = mean;
		this.eigenValues = eigenValues;		
	}
	
	public static PrincipalComponents read(File file) throws Exception {
		DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file))); 
			
		int nEV = in.readInt();
		int oDim = in.readInt();
	
		
		double[] means = new double[oDim];
		for ( int i=0; i<oDim; i++) {
			means[i] = in.readDouble();
		}
		
		double[][] eigenVectors = new double[nEV][oDim];
		for ( int ir=0; ir<nEV; ir++) {
			for ( int ic=0; ic<oDim; ic++) {
				eigenVectors[ir][ic] = in.readDouble();
			}
		}
		
		double[] eigenValues = null;
		boolean eigenValues_flag = in.readBoolean();
			
			if ( eigenValues_flag ) {
				eigenValues = new double[nEV];  
				for ( int i=0; i<nEV; i++) {
					eigenValues[i] = in.readDouble();
				}
			}
		
		
		return	new PrincipalComponents(eigenVectors, means, eigenValues);		
		
	}
	
	public void save(File file) throws IOException {
		DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file))); 
		
		int nEV =eigenValues.length;
		int oDim = means.length;
		
		out.writeInt(nEV);
		out.writeInt(oDim);
		
		for ( int i=0; i<oDim; i++) {
			out.writeDouble(means[i]);
		}
		
		for ( int ir=0; ir<nEV; ir++) {
			for ( int ic=0; ic<oDim; ic++) {
				out.writeDouble(eigenVectors[ir][ic]);
			}
		}
		
		if ( eigenValues == null ) {
			out.writeBoolean(false);
		} else {
			out.writeBoolean(true);
			for ( int i=0; i<nEV; i++) {
				out.writeDouble(eigenValues[i]);
			}
		}
		
		out.close();
	}
	

	
	public final double[] project(double[] data, int nComp) {
		double[] res = new double[nComp];
		int n=data.length;
		for ( int k=0; k<nComp; k++) {
			double scalarProduct=0;
			double[] vk=eigenVectors[k];
			for(int i1=0; i1<n; i1++) {
				scalarProduct+=vk[i1]*(data[i1]-means[i1]);
			}
			res[k]=scalarProduct;
		
		}		
		return res;
	}
	

	
	public void withening(double[] given, double scalar) {
		for ( int i=0; i<given.length; i++ ) {
			given[i] = scalar*( given[i] / Math.sqrt(eigenValues[i]));
		}
		
	}
	
	
}
