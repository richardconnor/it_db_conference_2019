package lmds;

import org.opencv.core.Core;

public class TestLandmarkMSD {



	static { System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	System.out.println("Welcome to OpenCV " + Core.VERSION);
	}


	  public static void main(String[] args) throws Exception {
		  System.out.println("Welcome to OpenCV " + "all commented out by al" );
//	    Metric<CartesianPoint> metric = new Euclidean<>();
//	    ArrayList <CartesianPoint> refPoints= new ArrayList();
////	    double [][] objs= {
////	    		{0.5131 ,  -1.3416 ,   0.2513  ,  0.4221 ,  -0.1412  ,  0.5585 ,   0.3165 ,  -0.5152  , -0.5694 ,  -0.9302},
////	    		{ -0.4794,   -1.5502,    1.7465,   -1.0634,   -0.5626,    1.0568,    0.2793,   -0.3385,   -3.0997,   -2.4154},
////	    		{-1.4826,    6.4055,   -2.2926,   -0.5416,    0.6963,   -2.5395,   -1.2370,    2.2833,    5.2319,    5.2665},
////	    		{     0.9819,   -5.7735,    1.7660,   -1.1118,   -1.6375,    2.0293,    0.7095,   -2.1884,   -5.7056,   -5.1974}
////	    		};
////	    double [][] tObj= {{3.4213,    1.2337 ,   2.1098 , -4.3404 ,   0.8628  , -1.5956  ,  0.1099  , -2.9695 ,   1.8239 ,   2.3641},
////				 { 0.4899,    1.0334,    0.7236,   -1.1044,   -0.1640,   -0.6135,    0.6157,    0.1539,    1.9327,    1.3770},
////				 { 0.0267,    0.7795,    0.4979,   -1.2542,    0.6495,   -0.2886,    0.4335,    0.7697,    0.4844,    1.2231}
////		 };
//	    //ex2
//	    double [][] objs= {
//	    		{0.5131 ,  -1.3416 ,   0.2513  ,  0.4221  },
//	    		{ -0.4794,   -1.5502,    1.7465,   -1.0634},
//	    		{-1.4826,    6.4055,   -2.2926,   -0.5416},
//	    		{     0.9819,   -5.7735,    1.7660,   -1.1118}
//	    		};
//	    double [][] tObj=
//	    	    {{3.4213,    1.2337 ,   2.1098 , -4.3404 },
//				 { 0.4899,    1.0334,    0.7236,   -1.1044},
//				 { 0.0267,    0.7795,    0.4979,   -1.2542}
//		 };
//	    System.out.println("lamdmark points");
//	    printMat( objs);
//	    System.out.println("");
//	    for(int i1=0; i1<objs.length; i1++) {
//	    	CartesianPoint point=new CartesianPoint(objs[i1]);
//	    	refPoints.add(point);
//	    }
//		 System.out.println("test obj");
//		 printMat(tObj);
//		 System.out.println("");
////	    long start = System.nanoTime();
//	    LMDS lmds= new LMDS( metric, refPoints, 3);
////	    long finish = System.nanoTime();
////	    long timeElapsed = finish - start;
////		 System.out.println("MDS base time "+timeElapsed);
//
////		 System.out.println("k:" +lmds.getK());
////		 System.out.println("Eigenvectors:");
////		 printMat( lmds.getPc().eigenVectors);
////		 System.out.println("Eigenvalues:");
////		 printVec(lmds.getPc().eigenValues);
////		 System.out.println("means:");
////		 printVec( lmds.getPc().means);
//
//		 System.out.println("Projected landmark(rows):");
//		 double [][] projectedLandmark= lmds.computeProjection(refPoints);
//		 printMat( projectedLandmark);
//		    System.out.println("");
//
//		 System.out.println(" projected test obj");
//		 ArrayList <CartesianPoint> tObjList= new ArrayList();
//		 for(int i1=0; i1<tObj.length; i1++) {
//		    	CartesianPoint point=new CartesianPoint(tObj[i1]);
//		    	tObjList.add(point);
//		    }
//		 double [][] projectedTestObjs= lmds.computeProjection(tObjList);
//		 printMat(projectedTestObjs);
//		 System.out.println("");
//
//		 System.out.println("orig dist Landmarks \t - \t euclid dist proj Landmarks");
//		 for(int i1=0; i1<tObj.length; i1++) {
//			 for(int i2=i1+1; i2<tObj.length; i2++) {
//				  double actualDist=metric.distance(refPoints.get(i1), refPoints.get(i2));
//				  double projDist= l2(projectedLandmark[i1],projectedLandmark[i2]);
//				  System.out.println(actualDist+" \t - \t "+projDist);
//			 }
//		 }
//		System.out.println("");
//
//		 System.out.println("orig dist test objs \t - \t euclid dist test objs");
//		 for(int i1=0; i1<tObj.length; i1++) {
//			 for(int i2=i1+1; i2<tObj.length; i2++) {
//				  double actualDist=metric.distance(tObjList.get(i1), tObjList.get(i2));
//				  double projDist= l2(projectedTestObjs[i1],projectedTestObjs[i2]);
//				  System.out.println(actualDist+" \t - \t "+projDist);
//			 }
//
//
//		    }
//
	    
	  }


		private static void printMat(double[][] matrix) {
		int nRows=matrix.length;
		int nCols=matrix[0].length;
		System.out.print("[");
		for(int i1=0; i1<nRows; i1++) {
			for(int i2=0; i2<nCols-1; i2++) {
				System.out.print(matrix[i1][i2]+", ");
			}
			if(i1==nRows-1)
				System.out.println(matrix[i1][nCols-1]+"]");	
			else
				System.out.println(matrix[i1][nCols-1]+";");
		}
	}
	private static void printVec(double[]vec) {
		int n=vec.length;
		System.out.print("[");
		for(int i1=0; i1<n-1; i1++) {
			System.out.print(vec[i1]+", ");
		}
		System.out.println(vec[n-1]+"]");
	}
	
	public static double l2(double[] xs, double[] ys) {
		double acc = 0;
		for (int i=0; i< xs.length; i++) {//no check--just for fast test
			double diff = xs[i] - ys[i];
			acc += diff * diff;
		}
		return Math.sqrt(acc);
	}
	
}
