package lmds;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public class TestMSD {

	static { System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
	
	
	  public static void main(String[] args) throws Exception {
	    System.out.println("Welcome to OpenCV " + Core.VERSION);
	    Mat m = new Mat(4, 4, CvType.CV_64FC1, new Scalar(0)); //matrxi squared distance
	    System.out.println("OpenCV Mat: " + m);
	  //  test1
//	    m.put(0, 0, 0); m.put(0, 1, 1); m.put(0, 2, 1); m.put(0, 3, 1);
//	    m.put(1, 0, 1); m.put(1, 1, 0); m.put(1, 2, 1); m.put(1, 3, 1);
//	    m.put(2, 0, 1); m.put(2, 1, 1); m.put(2, 2, 0); m.put(2, 3, 1);
//	    m.put(3, 0, 1); m.put(3, 1, 1); m.put(3, 2, 1); m.put(3, 3, 0);
//	    //test2
//	    m.put(0, 0, 0); m.put(0, 1, 3.1416); m.put(0, 2, 0.7854); m.put(0, 3, 1.5708);
//	    m.put(1, 0, 3.1416); m.put(1, 1, 0); m.put(1, 2, 2.3562); m.put(1, 3, 1.5708);
//	    m.put(2, 0, 0.7854); m.put(2, 1, 2.3562); m.put(2, 2, 0); m.put(2, 3, 2.3562);
//	    m.put(3, 0, 1.5708); m.put(3, 1, 1.5708); m.put(3, 2, 2.3562); m.put(3, 3, 0);
	   //test3
	    m.put(0, 0, 0); m.put(0, 1, 14.5377); m.put(0, 2, 163.9979); m.put(0, 3, 76.4538);
	    m.put(1, 0, 14.5377); m.put(1, 1, 0); m.put(1, 2, 233.0042); m.put(1, 3, 40.2122);
	    m.put(2, 0, 163.9979); m.put(2, 1, 233.0042); m.put(2, 2, 0); m.put(2, 3, 450.4242);
	    m.put(3, 0, 76.4538); m.put(3, 1, 40.2122); m.put(3, 2, 450.4242); m.put(3, 3, 0);
	  
	    System.out.println("OpenCV Mat data:\n" + m.dump());
	    long start = System.nanoTime();
	    PrincipalComponents pc=MDS_OpenCV.compute(m);
	    long finish = System.nanoTime();
		 long timeElapsed = finish - start;
		 System.out.println("time "+timeElapsed);
		 
	    System.out.println("Eigenvectors:");
	    printMat( pc.eigenVectors);
	    System.out.println("Eigenvalues:");
	    printVec(pc.eigenValues);
	    System.out.println("means:");
	    printVec(pc.means);
	    System.out.println("Projected points (trasposed):");
	    // eigenvector* sqrt(eigenvalues)
	    for(int k=0; k< pc.eigenValues.length; k++) {
	    	double eigVal=pc.eigenValues[k];
	    	if(eigVal>Math.pow(10, -10)) {
	    		System.out.print("[");
	    		double[] eigVec=pc.eigenVectors[k];
	    		for (int j=0; j<eigVec.length-1; j++) {
	    			double val=eigVec[j]*Math.sqrt(eigVal);
	    			System.out.print(val+",");
	    		}
	    		double val=eigVec[eigVec.length-1]*Math.sqrt(eigVal);
    			System.out.println(val+"]");
	    	}
	    }
	    
	    
	    
	  }


		private static void printMat(double[][] matrix) {
		int nRows=matrix.length;
		int nCols=matrix[0].length;
		System.out.print("[");
		for(int i1=0; i1<nRows; i1++) {
			for(int i2=0; i2<nCols-1; i2++) {
				System.out.print(matrix[i1][i2]+", ");
			}
			if(i1==nRows-1)
				System.out.println(matrix[i1][nCols-1]+"]");	
			else
				System.out.println(matrix[i1][nCols-1]+";");
		}
	}
	private static void printVec(double[]vec) {
		int n=vec.length;
		System.out.print("[");
		for(int i1=0; i1<n-1; i1++) {
			System.out.print(vec[i1]+", ");
		}
		System.out.println(vec[n-1]+"]");
	}
	
	
}
