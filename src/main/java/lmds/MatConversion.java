package lmds;

import org.opencv.core.Mat;

/**
 * @author lucia vadicamo
 *
 */
public class MatConversion {
	public static final double[][] getDoubleMatrix(Mat m) {
		int r = m.rows();
		int c = m.cols();
		
		double[][] res = new double[m.rows()][m.cols()];
		
		for ( int ir=0; ir<r; ir++) {
			for ( int ic=0; ic<c; ic++) {
				res[ir][ic] = m.get(ir, ic)[0];
			}
		}
		return res;
	}
	
public static final double[] getDoublesRows(Mat m ) {
		
		double[] res = new double[m.rows()];
		
		int c = m.rows();
		
		for ( int ic=0; ic<c; ic++) {
			res[ic] = m.get(ic, 0)[0];
		}
		return res;
	}
}
