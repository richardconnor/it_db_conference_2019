package lmds;

import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import stringVectorisation.Metric;


/**
 * @author Lucia
 [1] De Silva, Vin, and Joshua B. Tenenbaum. Sparse multidimensional scaling using landmark points. Vol. 120. Technical report, Stanford University, 2004.
http://www.cmap.polytechnique.fr/~peyre/cours/x2005signal/dimreduc_landmarks.pdf

LMDS
Goal: embed an object $s$ into $R^{k}$ given the distances of $s$ to a set of landmark points (pivots) p1, ..pn

Steps:
1) select landmark points (in [1] the autors suggest 2  two ways of selecting the landmark set (random and MaxMin). 
This Java class is not implementing the pivot selection since Random choice works quite well in practice)


2) Apply classical MDS to find a k � n matrix L representing an embedding of the n landmark
points in R^k [As input, use the n � n matrix Dn of distances between pairs of landmark
points]

3)Apply distance-based triangulation to find a k dim vector representing the embedding of the 
data points $s$ in R^k
[As input, use the n-dim vector of distances between landmark points and the data point. 
The new coordinates are derived from the squared distances by an affine linear transformation.

4)Recenter the data about their mean, and use PCA to align the principal axes of the newly-embedded 
data with the coordinate axes, in order of decreasing significance.

Notes:
The final PCA stage (step 3)  is not essential, but it normalises the output.In addition, if k'< k
then a good embedding in R^k'is obtained by restricting to the first k' coordinates of the
PCA-aligned embedding. It is better to use a higher value of k and restrict to k'-dimensions
at the end, than to use a low value of k throughout.

For a k-dimensional embedding, we require at least k+1 landmarks.  It is generally advisable
to to choose rather more landmark points than the strict minimum.
 *
 */
public class LMDS<T>  {

	static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
	
	private List<T> landmarkPoints;	
	private Metric<T> metric;
	private PrincipalComponents pc;
	private int k;
	
	
	
	/**
	 * @return the landmarkPoints
	 */
	public List<T> getLandmarkPoints() {
		return landmarkPoints;
	}


	/**
	 * @return the metric
	 */
	public Metric<T> getMetric() {
		return metric;
	}


	/**
	 * @return the pc
	 */
	public PrincipalComponents getPc() {
		return pc;
	}


	/**
	 * @return the k
	 */
	public int getK() {
		return k;
	}


	public LMDS(Metric<T> metric, List<T> refPoints, int reducedDim) throws Exception   {
		//For a k-dimensional embedding, we require at least k+1 landmarks
		this.metric = metric;
		this.landmarkPoints = refPoints;
		this.pc=msdOnLandmarks();//STEP 2:Apply classical MDS to find a k � n matrix L representing an embedding of the n landmark points in R^k 		
	    int numerOfPositiveEigenvalues=0;
	    for(int i1=0; i1<pc.eigenValues.length; i1++){
	    	if(pc.eigenValues[i1]>Math.pow(10, -13)) numerOfPositiveEigenvalues++;
	    }
	    this.k = Math.min(refPoints.size()-1,Math.min(reducedDim, numerOfPositiveEigenvalues));
	    
	    if(this.k< reducedDim) {
	    	final LmdsError lmdsError = new LmdsError("Not sufficient landmark points to project in " + reducedDim +" dims (for the chosen landmark points tha max dim is" + this.k+")");
	    	//LMDS lmds= new LMDS (metric, List<T> refPoints, int reducedDim)
	    	lmdsError.setLmds(this);
//	    	lmdsError.setEigenValues(pc.eigenValues);
//	    	lmdsError.setnPosEig(numerOfPositiveEigenvalues);
	    	throw lmdsError;
	    	}
	  
	}
	
	
/**
 * Computing STEP 2. i.e., apply classical MDS to find a k � n matrix L representing an embedding of the n landmark points in R^k 		
 * 
 * @throws Exception
 */
	private PrincipalComponents msdOnLandmarks() throws Exception {
		int nLP=landmarkPoints.size();
		
		//compute the matrix DN^2=[d(p_i, p_j)^2]
		Mat squaredDistMatrix =  new Mat(nLP, nLP, org.opencv.core.CvType.CV_64F);
		for(int o1=0; o1<nLP; o1++) {
			T curr1=landmarkPoints.get(o1); 
			for(int o2=o1+1; o2<nLP;o2++) {
				T curr2=landmarkPoints.get(o2);  
				double dist=metric.distance(curr1, curr2);
				squaredDistMatrix.put(o1, o2, dist*dist);
				squaredDistMatrix.put(o2, o1, dist*dist);
			}
		}
		//apply classical MDS		
		return(MDS_OpenCV.compute(squaredDistMatrix));
		
	}

	
///**
// * @param dimension the dimension to set
// */
//public void setDimension(int dimension) {
//	this.dimension = dimension;
//}
	
	
	/**
	 * computing STEP 3, i.e. Apply distance-based triangulation to find a k dim vector representing the embedding of the 
data points $s$ in R^k. As input, use the n-dim vector of distances between landmark points and the data point.  
The new coordinates are derived from the squared distances by an affine linear transformation.
	 * @param p
	 * @return
	 */
	public double[] computeProjection(T p) {
		double[] squareddists = new double[this.landmarkPoints.size()];
		for (int i1 = 0; i1 < squareddists.length; i1++) {
			double dist=this.metric.distance(p, this.landmarkPoints.get(i1));
			squareddists[i1] = dist*dist;
		}
		double[] temp = pc.project(squareddists,k);
		pc.withening(temp, -0.5);
		return temp;
	}
	
	public double[][] computeProjection(List<T> pList) {
		int npoints=pList.size();
		int nDim=k;
		
		double[][]res=new double[npoints][k];
		for(int ip=0; ip<npoints; ip++) {
			res[ip]=computeProjection(pList.get(ip));
		}
		return res;
	}
	
	public static class LmdsError extends Exception {
		LMDS lmds;


		public LmdsError(String string) {
			super(string);
		}

		/**
		 * @return the lmds
		 */
		public LMDS getLmds() {
			return lmds;
		}

		/**
		 * @param lmds the lmds to set
		 */
		public void setLmds(LMDS lmds) {
			this.lmds = lmds;
		}
		
		
	}


}
