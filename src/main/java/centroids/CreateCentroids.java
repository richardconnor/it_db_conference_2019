package centroids;

import stringVectorisation.Metric;

import stringVectorisation.NdimSimplex;
import stringVectorisation.JS;
import stringVectorisation.Levenshtein;
import stringVectorisation.StringMisc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CreateCentroids {
	private static String dicBase = "/Users/newrichard/git/MetricSpaceFramework/MetricSpaceFramework/sisap_data/strings/dictionaries/";

	public static void main(String[] a) throws IOException, NdimSimplex.SimplexError {
//		simpleTest();

		final LevSqrt levsq = new LevSqrt();
		final Levenshtein lev = new Levenshtein();
		final JS jsd = new JS(256);
		Metric<String> metric = levsq;
		List<String> dict = StringMisc.getDict(dicBase);

		long t0 = System.currentTimeMillis();
//		getNNs(metric, dict, dict);
		long t1 = System.currentTimeMillis();
		System.out.println(t1 - t0);

		List<double[]> vs = createVectorSpace(metric, dict, 50);

		t0 = System.currentTimeMillis();
//		getNNs(getL2metric(), vs, dict);
		t1 = System.currentTimeMillis();
		System.out.println(t1 - t0);
	}

	private static Metric<double[]> getL2metric() {

		return new Metric<double[]>() {

			@Override
			public double distance(double[] x, double[] y) {
				return NdimSimplex.l2(x, y);
			}

			@Override
			public String getMetricName() {
				return "l2";
			}
		};
	}

	// Doesnt compile - VPT and Metric incompatible
//	private static <T> void getNNs(Metric<T> metric, List<T> dict, List<String> orig) {
//		VPTree<T> vpt = new VPTree<>(dict, metric);
//
//		for (int i = 0; i < 10; i++) {
//			List<Integer> res = vpt.nearestNeighbour(dict.get(i), 5);
//			for (int r : res) {
//				System.out.print(orig.get(r) + "\t");
//			}
//			System.out.println();
//		}
//		System.out.println();
//	}

	private static <T> List<double[]> createVectorSpace(Metric<T> metric, List<T> data, int dim) throws NdimSimplex.SimplexError {
		Random r = new Random();
		List<T> refs = new ArrayList<>();
		for (int i = 0; i < dim; i++) {
			refs.add(data.get(r.nextInt(data.size())));
		}
		NdimSimplex<T> simp = new NdimSimplex<>(metric, refs);
		List<double[]> res = new ArrayList<>();
		for (T dat : data) {
			res.add(simp.getApex(dat));
		}
		return res;
	}

	private static void simpleTest() throws NdimSimplex.SimplexError {
		String[] egs = { "hello", "world", "how", "are", "you" };

		List<String> list = new ArrayList<>();
		for (String s : egs) {
			list.add(s);
		}

		final LevSqrt lev = new LevSqrt();
		final JS jsd = new JS(256);
		Metric<String> metric = jsd;
		NdimSimplex<String> simp = new NdimSimplex<>(metric, list);

		String ega = "antidisestablishmentarianism";
		String egb = "kangarooballs";

		System.out.println("true distance is " + metric.distance(ega, egb));

		double[] ap1 = simp.getApex(ega);
		double[] ap2 = simp.getApex(egb);

		double lwb = NdimSimplex.l2(ap1, ap2);
		System.out.println("lower bound is " + lwb);

		double[] bounds = NdimSimplex.getBounds(ap1, ap2);

		System.out.println("upper bound is " + bounds[1]);
		System.out.println("estimator is " + (bounds[0] + bounds[1]) / 2);
	}

}
