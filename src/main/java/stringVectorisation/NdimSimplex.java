package stringVectorisation;

import java.util.ArrayList;
import java.util.List;

public class NdimSimplex<T> {

	public static class SimplexError extends Exception {
		private double[] res;
		private int badStep;

		public SimplexError(String string) {
			super(string);
		}

		public double[] getRes() {
			return this.res;
		}

		public void setRes(double[] res) {
			this.res = res;
		}

		public int getBadStep() {
			return this.badStep;
		}

		public void setBadStep(int badStep) {
			this.badStep = badStep;
		}

	}

	private static double epsilon = 1e-10;

	public static double[] getBounds(double[] apex1, double[] apex2) {
		double[] res = new double[2];
		int l = apex1.length;
		assert l == apex2.length;

		double acc = 0;
		for (int i = 0; i < l - 1; i++) {
			double diff = apex1[i] - apex2[i];
			acc += diff * diff;
		}
		double lastDiff = apex1[l - 1] - apex2[l - 1];
		double lastSum = apex1[l - 1] + apex2[l - 1];
		res[0] = Math.sqrt(acc + lastDiff * lastDiff);
		res[1] = Math.sqrt(acc + lastSum * lastSum);
		return res;
	}

	public static double getZenith(double[] apex1, double[] apex2) {
		int l = apex1.length;
		assert l == apex2.length;

		double acc = 0;
		for (int i = 0; i < l - 1; i++) {
			double diff = apex1[i] - apex2[i];
			acc += diff * diff;
		}
		double alt1 = apex1[l - 1];
		double alt2 = apex2[l - 1];

		return Math.sqrt(acc + alt1 * alt1 + alt2 * alt2);
	}

	public static double l2(double[] xs, double[] ys) {
		double acc = 0;
		int max = xs.length;
		for (int i = 0; i < max; i++) {
			double diff = xs[i] - ys[i];
			acc += diff * diff;
		}
		return Math.sqrt(acc);
	}

	public static double l2(double[] xs, double[] ys, int dims) {
		double acc = 0;
		for (int i = 0; i < dims; i++) {
			double diff = xs[i] - ys[i];
			acc += diff * diff;
		}
		return Math.sqrt(acc);
	}

	public static double l2Flex(double[] xs, double[] ys) {
		double acc = 0;
		final int smallerDim = Math.min(xs.length, ys.length);
		for (int i = 0; i < smallerDim; i++) {
			// for (int i : Range.range(0, Math.min(xs.length, ys.length))) {
			double diff = xs[i] - ys[i];
			acc += diff * diff;
		}
		return Math.sqrt(acc);
	}

	private static double[] getApex(double[][] points, double[] distances) throws SimplexError {
		int dimension = distances.length;
		double res[] = new double[dimension];
		res[0] = distances[0];
		assert res[0] > 0;
		for (int i = 1; i < dimension; i++) {
			double l2 = l2Flex(res, points[i]); // +ve or zero - zero is completely reasonable!

			double d = distances[i];
			/*
			 * if this is zero, this means that the query point is equal to one already used
			 * to construct the base of the simplex currently under construction. This
			 * should be treated as an error during base simplex construction, but should be
			 * allowed for object mapping in general as an object equal to one of the
			 * simplex constructors may occur.
			 */

			if (d <= epsilon) {
				final SimplexError simplexError = new SimplexError("distance too small in getApex at step " + i);
				simplexError.setRes(res);
				throw simplexError;
			}
			double xN = points[i][i - 1];
			/*
			 * this will always be positive or zero, as it represents the altitude of the
			 * representation of the base simplex apex at the immediate level of adjustment.
			 * If the number of points is greater than the "inherent dimensionality",
			 * whatever that means, this it will reduce to zero. At that stage it means that
			 * the points within the simplex are no longer orthogonal. This in turn means
			 * that the apex has been fully mapped into the current dimension.
			 * 
			 * So, when calculating X_n at the previous iteration, if it is less than
			 * epsilon we will terminate the computation leaving all the other result vector
			 * values as zero. If this happens during simplex construction, it should be an
			 * error; if during object mapping, it isn't
			 */

			double yN = res[i - 1];// no constraint
			/*
			 * don't need to worry about X_n being very small as this can't happen as the
			 * exception will already have been raised
			 */
			double secondLastVal = yN - (d * d - l2 * l2) / (2 * xN);

			res[i - 1] = secondLastVal;
			// assert yN * yN >= secondLastVal * secondLastVal : yN + ":"
			// + secondLastVal + " (" + i + "th dimension";

			double lastVal = Math.sqrt(yN * yN - secondLastVal * secondLastVal);

			if (Double.isNaN(lastVal) || lastVal < epsilon) {
				/*
				 * we are about to add a new altitude that is either zero, is indistinguishable
				 * from zero with rounding errors in floating point arithmetic, or has been
				 * calculated as the square root of a negative number again due to rounding
				 * errors; in any case, the new point is effectively co-planar and tehre is no
				 * more resolution that can be achieved - rounding errors can go ballistic and
				 * generate huge numbers if this isn't caught!
				 */
				final SimplexError simplexError = new SimplexError("added non-orthoganal apex at step " + i);
				simplexError.setRes(res);
				throw simplexError;
			}

			res[i] = lastVal;

		}
		return res;
	}

	private List<T> referencePoints;

	// in fact a lower triangular matrix
	private double[][] baseSimplex;

	private Metric<T> metric;

	private int dimension;

	public NdimSimplex(Metric<T> metric, List<T> refPoints) throws SimplexError {
		this.dimension = refPoints.size();
		this.metric = metric;
		this.referencePoints = refPoints;

		initialise();

	}

	public double[] getApex(double[] dists) {
		try {
			return getApex(this.baseSimplex, dists);
		} catch (SimplexError e) {
			return (e.getRes());
		}
	}

	public double[] getApex(T p) {
		double[] dists = new double[this.referencePoints.size()];
		for (int i = 0; i < dists.length; i++) {
			dists[i] = this.metric.distance(p, this.referencePoints.get(i));
		}
		try {
			return getApex(this.baseSimplex, dists);
		} catch (SimplexError e) {
			return (e.getRes());
		}
	}

	private void initialise() throws SimplexError {
		this.baseSimplex = new double[this.dimension][this.dimension - 1];
		// form the inductive base case
		this.baseSimplex[1][0] = this.metric.distance(this.referencePoints.get(0), this.referencePoints.get(1));

		if (Math.abs(this.baseSimplex[1][0]) <= epsilon) {
			throw new SimplexError("first two objects are too close");
		}

		// for every further reference point, add the apex
		for (int i = 2; i < this.referencePoints.size(); i++) {
			// distances to all previous points
			double[] dists = new double[i];
			for (int j = 0; j < i; j++) {
				dists[j] = this.metric.distance(this.referencePoints.get(i), this.referencePoints.get(j));

				if (dists[j] <= epsilon) {
					final SimplexError simplexError = new SimplexError("distance too small in getApex at step " + i);
					simplexError.setBadStep(i);
					throw simplexError;
				}
			}

			try {
				double[] apex = getApex(this.baseSimplex, dists);
				this.baseSimplex[i] = apex;
			} catch (SimplexError se) {
				final SimplexError simplexError = new SimplexError("non-orthogonal addition in getApex at step " + i);
				simplexError.setBadStep(i);
				throw simplexError;
			}
		}
	}
}
