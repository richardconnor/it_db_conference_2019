package stringVectorisation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import local_runners_and_dir_links.LocalDirectoryLinks;
import stringVectorisation.NdimSimplex.SimplexError;

public class ErrorTest {

	public static final String[] metrics = { "lev", "jsd" };
	// public static final String[] metrics = { "lev" };
	public static final String[] experiments = { "cheb", "euc", "simpLwb", "simpZen" };
	// public static final String[] experiments = { "simpLwb", "simpZen" };
	public static final int[] dims = { 4, 8, 16, 32 };

	private String rootOutputDir;
	private List<String> testLoad;

	public ErrorTest(List<String> testLoad, String rootOutputDir) {
		this.testLoad = testLoad;
		this.rootOutputDir = rootOutputDir;
	}

	public void refreshTestLoad(List<String> testLoad) {
		this.testLoad = testLoad;
	}

	public void runTestsForPaper() throws FileNotFoundException, IOException {
		final LevSqrt levsq = new LevSqrt();
		final Levenshtein lev = new Levenshtein();
		final JS jsd = new JS(256);
		final Metric<double[]> chebyshev = new Chebyshev();
		final Metric<double[]> euc = new Metric<double[]>() {

			@Override
			public double distance(double[] x, double[] y) {
				return NdimSimplex.l2(x, y);
			}

			@Override
			public String getMetricName() {
				return "euc";
			}
		};

		boolean nsimp = true;
		Metric<String> trueDistance;
		Metric<double[]> proxyMetric = euc;
		boolean squareRootDistance = true;
		boolean zenith = true;

		for (int noOfPivots : dims) {
			for (String m : metrics) {
				trueDistance = m.equals("lev") ? lev : jsd;
				for (String experiment : experiments) {
					switch (experiment) {
					case "cheb": {
						nsimp = false;
						proxyMetric = chebyshev;
						squareRootDistance = false;
					}
						;
						break;
					case "euc": {
						nsimp = false;
						proxyMetric = euc;
						squareRootDistance = false;
					}
						;
						break;
					case "simpLwb": {
						nsimp = true;
						zenith = false;
						squareRootDistance = false;
						if (m.equals("lev")) {
							trueDistance = levsq;
							squareRootDistance = true;
						}
					}
						;
						break;
					case "simpZen": {
						nsimp = true;
						zenith = true;
						squareRootDistance = false;
						if (m.equals("lev")) {
							trueDistance = levsq;
							squareRootDistance = true;
						}
					}
						;
						break;
					default: {
						throw new RuntimeException("this is an erroroneous experiment!");
					}
					}

					String fileName = getFileName(noOfPivots, m, experiment);

					System.out.println(fileName);
					final String newFileName = this.rootOutputDir + fileName;
					File f = new File(newFileName);
					if (!f.exists()) {
						PrintWriter pw = new PrintWriter(newFileName);

						for (int i = 0; i < 20; i++) {
							ErrorValues ev = doErrorValuesTest(nsimp, proxyMetric, trueDistance, squareRootDistance,
									zenith, noOfPivots);
							pw.println(
									ev.maxRatio + "," + ev.minRatio + "," + ev.meanRatio + "," + ev.getRelativeError()
											+ "," + ev.getAbsoluteSquaredError() + "," + ev.getDistortion());
							pw.flush();
						}
						pw.close();
					}
				}

			}
		}
	}

	public static String getFileName(int noOfPivots, String m, String exp) {
		String fileName = exp + m + noOfPivots;
		return fileName + ".csv";
	}

	private ErrorValues doErrorValuesTest(boolean nsimp, final Metric<double[]> projectedDistance,
			Metric<String> trueDistance, boolean sqProjected, boolean zenith, int noOfPivots) throws IOException {

		this.refreshTestLoad(StringMisc.getDict(LocalDirectoryLinks.dicBase));

		final int dataSize = 10000;

		List<String> data = StringMisc.removeSelection(this.testLoad, dataSize);
		List<String> pivots = StringMisc.removeSelection(this.testLoad, noOfPivots);

		NdimSimplex<String> nd = null;
		if (nsimp) {
			Random rand = new Random();
			while (nd == null) {
				/*
				 * extreme caution here...! if the space only accommodates a finite number of
				 * distances this will continually fail if the required dimensions are too high;
				 * even if it doesn't fail, there may be some kind of "over-fitting" effect I
				 * don't understand yet...
				 */
				try {
					nd = new NdimSimplex<>(trueDistance, pivots);
				} catch (SimplexError e) {
					int i = e.getBadStep();
					pivots.remove(i);
					pivots.add(this.testLoad.get(rand.nextInt(this.testLoad.size())));
				}
			}
		}

		List<double[]> projected = new ArrayList<>();
		for (int i = 0; i < data.size(); i++) {
			if (nsimp) {
				try {
					double[] apex = nd.getApex(data.get(i));
					projected.add(apex);
				} catch (Exception e) {
					System.out.println("couldn't convert " + data.get(i));
				}
			} else {
				double[] rep = new double[noOfPivots];
				for (int d = 0; d < noOfPivots; d++) {
					rep[d] = trueDistance.distance(data.get(i), pivots.get(d));
				}
				projected.add(rep);
			}
		}

		double[] trueDists = new double[dataSize * (dataSize - 1) / 2];
		double[] mappedDists = new double[dataSize * (dataSize - 1) / 2];

		int ptr = 0;
		for (int i = 0; i < data.size() - 1; i++) {
			for (int j = i + 1; j < data.size(); j++) {
				double d1 = trueDistance.distance(data.get(i), data.get(j));
				trueDists[ptr] = sqProjected ? d1 * d1 : d1;

				if (nsimp) {
					if (zenith) {
						double d2 = NdimSimplex.getZenith(projected.get(i), projected.get(j));
						mappedDists[ptr] = sqProjected ? d2 * d2 : d2;
					} else {
						double d2 = NdimSimplex.l2(projected.get(i), projected.get(j));
						if (sqProjected) {
							d2 = d2 * d2;
							if (Math.floor(d2) < d2) {
								d2 = Math.floor(d2) + 1;
							}
						}
						mappedDists[ptr] = d2;
					}
				} else {
					double d2 = projectedDistance.distance(projected.get(i), projected.get(j));
					mappedDists[ptr] = sqProjected ? d2 * d2 : d2;
				}
				ptr++;
			}
		}

		ErrorValues ev = new ErrorValues(trueDists, mappedDists);
		return ev;
	}

}
