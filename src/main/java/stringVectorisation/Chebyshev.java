package stringVectorisation;

public class Chebyshev implements Metric<double[]> {

	@Override
	public double distance(double[] x, double[] y) {
		double res = 0;
		for (int i = 0; i < x.length; i++) {
			res = Math.max(res, Math.abs(x[i] - y[i]));
		}
		return res;
	}

	@Override
	public String getMetricName() {
		return "cheby";
	}

}
