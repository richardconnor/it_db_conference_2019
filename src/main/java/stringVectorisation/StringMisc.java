package stringVectorisation;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StringMisc {

	public static List<String> getDict(String rootInputDir) throws IOException {
		List<String> res = new ArrayList<>();
		LineNumberReader f = new LineNumberReader(new FileReader(rootInputDir + "English.dic"));
		String line = f.readLine();
		while (line != null) {
			res.add(line);
			line = f.readLine();
		}

		f.close();
		return res;
	}

	public static List<String> removeSelection(List<String> collection, int n) {
		assert n < collection.size();
		List<String> res = new ArrayList<>();
		Random r = new Random();
		while (res.size() < n) {
			res.add(collection.remove(r.nextInt(collection.size())));
		}
		return res;
	}
}
