package stringVectorisation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lmds.LMDS;
import lmds.LMDS.LmdsError;
import stringVectorisation.NdimSimplex.SimplexError;

public class ErrorTestLucia {

	public static final String[] metrics = {"lev" ,"jsd" };
	public static final String[] exps = {"cheb", "euc","lmds", "simpLwb", "simpZen"};
	public static final int[] dims = {4,8,16,32,64};

	private String rootOutputDir;
	private List<String> testLoad;

	public ErrorTestLucia(List<String> testLoad, String rootOutputDir) {
		this.testLoad = testLoad;
		this.rootOutputDir = rootOutputDir;
	}

	public void runTestsForPaper(int ntries ) throws FileNotFoundException, IOException {
		final LevSqrt levsq = new LevSqrt();
		final Levenshtein lev = new Levenshtein();
		final JS jsd = new JS(256);
		final Metric<double[]> chebyshev = new Chebyshev();
		final Metric<double[]> euc = new Metric<double[]>() {

			@Override
			public double distance(double[] x, double[] y) {
				return NdimSimplex.l2(x, y);
			}

			@Override
			public String getMetricName() {
				return "euc";
			}
		};

		boolean nsimp = true;
		boolean lmds = false;
		Metric<String> trueDistance = levsq;
		Metric<double[]> nonSimpMetric = euc;
		boolean sqProjected = true;
		boolean zenith = true;

		for (int noOfPivots : dims) {
			for (String m : metrics) {
				trueDistance = m.equals("lev") ? lev : jsd;
				for (String exp : exps) {
					switch (exp) {
						case "lmds": {
							nsimp = false;
							nonSimpMetric = euc;
							sqProjected = false;
							lmds=true;
						}
							;
							break;
					case "cheb": {
						nsimp = false;
						nonSimpMetric = chebyshev;
						sqProjected = false;
						lmds=false;
					}
						;
						break;
					case "euc": {
						nsimp = false;
						nonSimpMetric = euc;
						sqProjected = false;
						lmds=false;
					}
						;
						break;
					case "simpLwb": {
						nsimp = true;
						zenith = false;
						lmds=false;
						if (m.equals("lev")) {
							trueDistance = levsq;
							sqProjected = true;
						}
					}
						;
						break;
					case "simpZen": {
						nsimp = true;
						zenith = true;
						lmds=false;
						if (m.equals("lev")) {
							trueDistance = levsq;
							sqProjected = true;
						}
					}
						;
						break;
					default: {
						//
					}
					}

					String fileName = getFileName(noOfPivots, m, exp);

					System.out.println(fileName);
					final String newFileName = this.rootOutputDir + fileName;
					File f = new File(newFileName);
					if (!f.exists()) {
						PrintWriter pw = new PrintWriter(newFileName);
						pw.println(
								"dims"+"," +"maxRatio"+ "," + "minRatio" + "," + "meanRatio" + "," + "RelativeError"
								+ "," + "AbsoluteSquaredError" + "," + "getDistortion"+ "," +"RelativeErrorNoScaling"
								+ "," + "AbsoluteSquaredErrorNoScaling");
						for (int i = 0; i < ntries; ) { 
							System.out.print((i+1)+"/"+ntries);

							ErrorValuesLucia ev;
							try {
								ev = doErrorValuesTest(nsimp,lmds, nonSimpMetric, trueDistance, sqProjected, zenith,
										noOfPivots);


								pw.println(
										ev.dims+","+ev.maxRatio + "," + ev.minRatio + "," + ev.meanRatio + "," + ev.getRelativeError()
										+ "," + ev.getAbsoluteSquaredError() + "," + ev.getDistortion()+ "," + ev.getRelativeErrorNoScaling()
										+ "," + ev.getAbsoluteSquaredErrorNoScaling());
								pw.flush();
								i++;
								System.out.print("\n");
							} catch (LmdsError e) {
								System.out.println("more sets of pivots were tested to find a suitable one for LMDS..");
							}catch (SimplexError e) {
								System.out.println("more sets of pivots were tested to find a suitable one for Nsimplex..");
							}catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
						pw.close();
					}
				}

			}
		}
	}

	public static String getFileName(int noOfPivots, String m, String exp) {
		String fileName = exp + m + noOfPivots;
		return fileName + ".csv";
	}

	private ErrorValuesLucia doErrorValuesTest(boolean nsimp, boolean doLMDS, final Metric<double[]> projectedDistance,
			Metric<String> trueDistance, boolean sqProjected, boolean zenith, int noOfPivots) throws Exception {

		ErrorValuesLucia ev=null;
		int ntries=0;

		int projDims=0;
		while(ev==null && ntries<10) {
			final int dataSize = 10000;
			List <String> testLoadcopy=new ArrayList<>(this.testLoad); 
			List<String> data = StringMisc.removeSelection(testLoadcopy, dataSize);
			List<String> pivots = StringMisc.removeSelection(testLoadcopy, noOfPivots);

			NdimSimplex<String> nd = null;
			LMDS<String>  lmds= null; 
			if (nsimp) {
				int simplexTries=1;
				Random rand = new Random();
				while (nd == null && simplexTries<1000) {
					/*
					 * extreme caution here...! if the space only accommodates a finite number of
					 * distances this will continually fail if the required dimensions are too high;
					 * even if it doesn't fail, there may be some kind of "over-fitting" effect I
					 * don't understand yet...
					 */
					try {
						nd = new NdimSimplex<>(trueDistance, pivots);
					} catch (SimplexError e) {
						int i = e.getBadStep();
						pivots.remove(i);
						pivots.add(this.testLoad.get(rand.nextInt(this.testLoad.size())));
						//System.out.print("+");
						simplexTries++;
					}
				}
				if(simplexTries>1) {
					System.out.print(", simplexTries:"+simplexTries);
					while (nd == null)  {
						try {
							nd = new NdimSimplex<>(trueDistance, pivots);
						} catch (SimplexError e) {
							int i = e.getBadStep();
							pivots.remove(i);
						}

					}
					System.out.print("- Projecting to "+ pivots.size() +" dims");
				}
			}

			if (doLMDS) {
				LMDS bestLmds=null;
				int bestdiff= pivots.size()-1;
				Random rand = new Random();
				int lmdsTries=1;
				while (lmds == null && lmdsTries<1000) {
					try {
						lmds= new LMDS(trueDistance, pivots, pivots.size()-1);
					} catch (LmdsError e) {
						LMDS reducdLmds=e.getLmds();
						int diff=pivots.size()-1-reducdLmds.getK();
						if(diff<bestdiff) {
							bestdiff=diff;
							bestLmds=reducdLmds;
						}
						pivots.remove(0);
						pivots.add(this.testLoad.get(rand.nextInt(this.testLoad.size())));
						//System.out.print("+");
						lmdsTries++;
					}
				}
				if(lmdsTries>1) {
					System.out.print(", lmdsTries "+lmdsTries);

					if(lmds == null) {
						lmds=bestLmds;
						System.out.print(" !! Projecting to"+ lmds.getK() +" dims");
					}
				}
			}

			List<double[]> projected = new ArrayList<>();
			for (int i = 0; i < data.size(); i++) {
				if (nsimp) {
					try {
						double[] apex = nd.getApex(data.get(i));
						projected.add(apex);
					} catch (Exception e) {
						System.out.println("couldn't convert " + data.get(i));
					}
				} else {
					if (doLMDS) {
						double[] values=lmds.computeProjection(data.get(i));
						projected.add(values);
					}else {
						double[] rep = new double[noOfPivots];
						for (int d = 0; d < noOfPivots; d++) {
							rep[d] = trueDistance.distance(data.get(i), pivots.get(d));
						}
						projected.add(rep);
					}
				}
			}

			double[] trueDists = new double[dataSize * (dataSize - 1) / 2];
			double[] mappedDists = new double[dataSize * (dataSize - 1) / 2];
			projDims= projected.get(0).length;

			int ptr = 0;
			for (int i = 0; i < data.size() - 1; i++) {
				for (int j = i + 1; j < data.size(); j++) {
					double d1 = trueDistance.distance(data.get(i), data.get(j));
					if (nsimp) {
						double d2 = zenith ? NdimSimplex.getZenith(projected.get(i), projected.get(j))
								: NdimSimplex.l2(projected.get(i), projected.get(j));
						trueDists[ptr] = d1;
						mappedDists[ptr] = d2;

					} else {
						double d2 = projectedDistance.distance(projected.get(i), projected.get(j));
						trueDists[ptr] = sqProjected ? d1 * d1 : d1;
						mappedDists[ptr] = sqProjected ? d2 * d2 : d2;

					}
					ptr++;
				}
			}

			ev = new ErrorValuesLucia(trueDists, mappedDists, projDims);
			if(ev.minRatio<Math.pow(10, -6)) {
				ev=null;
				ntries++;
			}

		}
		if(ev==null)
			throw new Exception("ev.minRatio<Math.pow(10, -6)");
		return ev;
	}

	/**
	 * @param pivots
	 */
	private void print(List<String> pivots) {
		System.out.print("pivots: ");
		for(String s: pivots) {
			System.out.print(s+"\t");
		}
		System.out.print("\n");
	}


		private void print(double[] vec) {
			for(double s: vec) {
				System.out.print(s+"\t");
			}
			System.out.print("\n");
		}

	}


